#!/bin/sh

set -e

root='https://nyx-the-pooh.codeberg.page/wordvomit'
updated=$(date --iso-8601=seconds)

cat <<-EOF
	<?xml version="1.0" encoding="utf-8"?>
	<feed xmlns="http://www.w3.org/2005/Atom">
	<title>Asya'a Website</title>
	<link href="${root}"/>
	<link rel="self" href="${root}/feed.atom"/>
	<id>${root}</id>
	<updated>${updated}</updated>
EOF

encode() {
	old_lc_collate=$LC_COLLATE
	LC_COLLATE=C
	i=1
	length="${#1}"
	while [ "$i" -le "$length" ]; do
		c="$(echo "$1" | cut -c "$i")"
		case $c in
		[a-zA-Z0-9.~_-]) printf "%s" "$c" ;;
		' ') printf "%%20" ;;
		*) printf '%%%02X' "'$c" ;;
		esac
		i=$((i + 1))
	done

	LC_COLLATE=$old_lc_collate
}

for file in *.md; do
	entry="${file%.md}.html"
	date="$(stat -c '%Z' "$file" | xargs -I{} date -d @{} --iso-8601=seconds)"
	cat <<-EOF
		<entry>
		<title>${entry}</title>
		<link href="${root}/${entry}"/>
		<id>${root}/${entry}</id>
		<published>${date}</published>
		<updated>${updated}</updated>
		<author>
			<name>Asya Belshova</name>
			<email>asyaa@disroot.org</email>
		</author>
		<content type="text">
	EOF
	encode "$file"
	cat <<-EOF
		</content>
		</entry>
	EOF

done

echo '</feed>'
