#!/bin/sh
#
# generate an html index of a directory
#
# args: $1 (optional): Path to create the index.html relative to.
# out: STDOUT: HTML index of files in $path

path=${1:-$PWD}

cat - <<EOHTML
<html>
<head>
<link rel="alternate" type="application/atom+xml" href="feed.atom">
<title>Words</title>
<style>
table {
	text-align: left;
}
.middle-col {
	padding-left: 10px;
	padding-right: 10px;
}
td.right-col {
	text-align: right;
}
</style>
<link rel="stylesheet" href="style.css">
</head>
<body>
<div class="panel">
<h1 class="panel-head">Words</h1>
<table class="table table-striped table-condensed">
<tr><th class="left-col">File</th><th class="middle-col">Modified</th><th class="right-col">Size in kB</th></tr>
EOHTML


find "$path" -maxdepth 1 -name '*.html' -not -path "$PWD" -not -name 'index.html' \
-printf '<tr><td class="left-col"><a href="%f">%f</a></td><td class="middle-col">%TY-%Tm-%Td %TH:%TM</td><td class="right-col">%s</td></tr>\n' | sort

cat - <<EOHTML
</table>
</div>
<br/>
<br/>
</body>
<div><small>Last Modified: $(date +%c)</small></div>
<div><small>Atom Feed: <a href="feed.atom">feed.atom</a></small></div>
</html>
EOHTML
