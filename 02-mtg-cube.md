# Magic: The Gathering Cube

my [cube](https://www.mtggoldfish.com/articles/cube-goldberg-machine-what-is-a-cube)
is now in what i consider to be a playable state.
it's a 180 card legacy-like cube for 2-4 players, with no "unfair" decks.
the full list is on [my website](https://nyx-the-pooh.codeberg.page/cube)
or [cubecobra](https://cubecobra.com/cube/overview/small-fair)
(requires javascript).

---

edit 2023-12-13: temporarily removed cube description
and added cube page to my website
